import javax.swing.JFrame
import javax.swing.JLabel

fun main(args: Array<String>) {

    //command for rerunning the compiled program
    val ReRunCommand = arrayOf("java", "-jar", "AlwaysOpenProgram.jar")

    //open the main window
    reOpenWindow()

    //when the program is closed
    Runtime.getRuntime().addShutdownHook(Thread {
        //rerun the program
        Runtime.getRuntime().exec(ReRunCommand)
    })
}

fun reOpenWindow(){
    // Create a JFrame (window)
    val frame = JFrame("you cant close that")
    frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    frame.setSize(420, 420) // Set the window size
    frame.layout = null // Use default layout manager

    // Create a JLabel (text label)
    val label = JLabel("this is stupid")
    label.setBounds(160, 160, 100, 69) // Set label position and size

    // Add the label to the frame
    frame.add(label)

    // Make the frame visible
    frame.isVisible = true
}